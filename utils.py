import AST

types = {}
for operator in ['+', '-', '*', '/']:
    types[(operator,'int','int')] = 'int' if operator != '/' else 'float'
    types[(operator,'int','float')] = 'float'
    types[(operator,'float','int')] = 'float'
    types[(operator,'float','float')] = 'float'
types[('+','string','string')] = 'string'
types[('*','string','int')] = 'string'
for operator in ['==', '!=', '>', '<', '<=', '>=']:
    types[(operator,'int','int')] = 'int'
    types[(operator,'int','float')] = 'int'
    types[(operator,'float','int')] = 'int'
    types[(operator,'float','float')] = 'int'
    types[(operator,'string','string')] = 'int'

def addToClass(cls):
    def decorator(func):
        setattr(cls,func.__name__,func)
        return func
    return decorator


class TypeChecker:
    @addToClass(AST.Node)
    def check(self):
        raise Exception("check not defined in class " + self.__class__.__name__);

    @addToClass(AST.Expression)
    def check(self):
        try:
            self.type = types[self.operator, self.operandA.type, self.operandB.type]      
        except:
            raise Exception("operands types mismatch")
	  
    @addToClass(AST.Assignment)
    def check(self):
        t=set({('int','int'), ('float', 'float'), ('string', 'string'), ('int', 'float'), ('float', 'int')})
        if (self.variable.type, self.expression.type) not in t:
            raise Exception("operands types mismatch")
        
    @addToClass(AST.IfStatement)
    def check(self):
        if self.condition.operator not in ['==', '!=', '>', '<', '<=', '>=']:
            raise Exception("Expected condition")
    
    @addToClass(AST.IfElseStatement)
    def check(self):
        if self.condition.operator not in ['==', '!=', '>', '<', '<=', '>=']:
            raise Exception("Expected condition")
    
    @addToClass(AST.WhileStatement)
    def check(self):
        if self.condition.operator not in ['==', '!=', '>', '<', '<=', '>=']:
            raise Exception("Expected condition")
    

class mem:
    value=0
    @staticmethod
    def next():
        mem.value = mem.value + 1
        return "%mem" + str(mem.value)

def invert_operator(operator):
    r = {'>' : '<=',
        '<=' : '>',
        '==' : '!=',
        '!=' : '==',
        '<' : '>=',
        '>=' : '<'}   
    try:
        return r[operator]
    except:
        raise Exception("Expected comparsion operator")

class CodeGenerator:
    @addToClass(AST.Node)
    def eval(self):
        raise Exception("eval not defined in class " + self.__class__.__name__);

    @addToClass(AST.Const)
    def eval(self):
        return []

    @addToClass(AST.Variable)
    def eval(self):
        return []

    @addToClass(AST.Expression)
    def eval(self):
        self.name = mem.next()
        codeA = self.operandA.eval()
        codeB = self.operandB.eval()
        return codeA + codeB + \
            [self.name + "\t= " + self.operandA.name + " " + self.operator + " " + self.operandB.name]

    @addToClass(AST.Assignment)
    def eval(self):
        rightCode = self.expression.eval()
        return rightCode + [self.variable.name + "\t\t= " + self.expression.name]

    @addToClass(AST.IfStatement)
    def eval(self):
        codeA = self.condition.operandA.eval()
        codeB = self.condition.operandB.eval()
        self.instructions.prefix = self.prefix + len(codeA) + len(codeB) + 1
        instr = self.instructions.eval()
        label = self.prefix + len(codeA) + len(codeB) + len(instr) + 2
        return codeA + codeB + [
            "if" + invert_operator(self.condition.operator) + "\t  " + \
            self.condition.operandA.name + " " + \
            self.condition.operandB.name + " " + str(label) \
            ] + instr

    @addToClass(AST.IfElseStatement)
    def eval(self):
        codeA = self.condition.operandA.eval()
        codeB = self.condition.operandB.eval()
        self.if_instructions.prefix = self.prefix + len(codeA) + len(codeB) + 1
        if_instr = self.if_instructions.eval()
        self.else_instructions.prefix = self.if_instructions.prefix + len(if_instr) + 1
        else_instr = self.else_instructions.eval()
        label = self.prefix + len(codeA) + len(codeB) + len(if_instr) + 3
        end_label = self.prefix + len(codeA) + len(codeB) + len(if_instr) + len(else_instr) + 3
        return codeA + codeB + [
                "if" + invert_operator(self.condition.operator) + "\t  " + \
                self.condition.operandA.name + " " + \
                self.condition.operandB.name + " " + str(label) \
                ] + if_instr + ["if==\t  1 1 " + str(end_label)] + else_instr

    @addToClass(AST.WhileStatement)
    def eval(self):
        codeA = self.condition.operandA.eval()
        codeB = self.condition.operandB.eval()
        self.instructions.prefix = self.prefix + len(codeA) + len(codeB) + 1
        instr = self.instructions.eval()
        label = self.instructions.prefix + len(instr) + 2
        return codeA + codeB + [
            "if" + invert_operator(self.condition.operator) + "\t  " + \
            self.condition.operandA.name + " " + \
            self.condition.operandB.name + " " + str(label) \
            ] + instr + ["if==\t  1 1 " + str(self.instructions.prefix)]


    @addToClass(AST.InstructionList)
    def eval(self):
        self.head.prefix = self.prefix
        head_code = self.head.eval()
        self.tail.prefix = self.prefix + len(head_code)
        tail_code = self.tail.eval()
        return head_code + tail_code
