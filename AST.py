class Node:
    pass

class Const(Node):
    def __init__(self, type, value):
        self.type = type
        self.value = value
        self.name = str(value)

class Variable(Node):
    def __init__(self, type, name):
        self.type = type
        self.name = name

class Expression(Node):
    def __init__(self, operator, operandA, operandB):
        self.operator = operator
        self.operandA = operandA
        self.operandB = operandB
        self.check()

class Assignment(Node):
    def __init__(self, variable, expression):
        self.variable = variable
        self.expression = expression
        self.check()

class IfStatement(Node):
    def __init__(self, condition, instructions):
        self.condition = condition
        self.instructions = instructions
        self.check()

class IfElseStatement(Node):
    def __init__(self, condition, if_instructions, else_instructions):
        self.condition = condition
        self.if_instructions = if_instructions
        self.else_instructions = else_instructions
        self.check()

class WhileStatement(Node):
    def __init__(self, condition, instructions):
        self.condition = condition
        self.instructions = instructions
        self.check()
        
class InstructionList(Node):
    def __init__(self, head, tail):
        self.head = head
        self.tail = tail
