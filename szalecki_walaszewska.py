#!/usr/bin/env python3
import sys
import ply.lex as lex
import ply.yacc as yacc
import AST
import utils

literals = "{}()<>=;,+-*/"
tokens = ( "ID", "FLOAT", "INTEGER", "STRING",
           "TYPE", "IF", "ELSE", "WHILE", "EQ", "NEQ", "LE", "GE" );
t_ignore = ' \t'
precedence = (
   ("nonassoc", 'IFX'),
   ("nonassoc", 'ELSE'),
   ("nonassoc", '<', '>', 'EQ', 'NEQ', 'LE', 'GE'),
   ("left", '+', '-'),
   ("left", '*', '/') )
   
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_error(t):
    print("Illegal character at line %d: %s" % (t.lexer.lineno, t.value[0]))
    t.lexer.skip(1)
    sys.exit(1)
    
def p_error(p):
    print("Syntax error at line %d: token" % lexer.lineno, p.type)
    sys.exit(1)

def semantic_error(lineno, s):
    print("Semantic error at line %d: %s" % (lineno, s))
    sys.exit(1)

def t_FLOAT(t):
    r"\d+(\.\d*)|\.\d+"
    return t

def t_INTEGER(t):
    r"\d+"
    return t

def t_STRING(t):
    r'\"([^\\\n]|(\\.))*?\"'
    return t

def t_IF(t):
    r"if"
    return t

def t_ELSE(t):
    r"else"
    return t

def t_WHILE(t):
    r"while"
    return t

def t_LE(t):
    r"<="
    return t

def t_GE(t):
    r">="
    return t

def t_EQ(t):
    r"=="
    return t

def t_NEQ(t):
    r"!="
    return t

def t_TYPE(t):
    r"int|float|string"
    return t

def t_ID(t):
    r"[a-zA-Z_]\w*"
    return t


environment = {}

def p_program(p):
    """program : declarations instructions"""
    p[2].prefix = 0
    code = p[2].eval()
    i = 1
    for instr in code:
        print("%3d: %s"% (i, instr))
        i=i+1

def p_declarations(p):
    """declarations : declarations declaration 
                    | """

def p_declaration(p):
    """declaration : TYPE vars ';' """ # vars - variables list
    for var in p[2]:
        try:
            environment[var]
            semantic_error(p.lineno(1), "Redefinition of variable %s" % var)
        except KeyError:
            environment[var] = p[1]

def p_vars_rec(p):
    """vars : ID ',' vars"""
    p[0] = [p[1]] + p[3] 
    
def p_vars(p):
    """vars : ID """
    p[0] = [p[1]]

def p_instructions_rec(p):
    """instructions : instruction instructions"""
    p[0] = AST.InstructionList(p[1], p[2])

def p_instructions(p):
    """instructions : instruction"""
    p[0] = p[1]

def p_instruction(p):
    """instruction : assignment
                   | choice_instr
                   | while_instr """
    p[0] = p[1]

def p_assignment(p):
    """assignment : ID '=' expression ';' """
    try:
        p[0] = AST.Assignment(AST.Variable(environment[p[1]], p[1]), p[3])
    except KeyError:
        semantic_error(p.lineno(1), "Variable not defined")
    except Exception as e:
        semantic_error(p.lineno(1), e)

def p_expression(p):
    """expression : expression '+' expression
                  | expression '-' expression
                  | expression '*' expression
                  | expression '/' expression """
    p[0] = AST.Expression(p[2], p[1], p[3])

def p_expression_id(p):
    """expression : ID"""
    try:
        p[0] = AST.Variable(environment[p[1]], p[1])
    except KeyError:
        semantic_error(p.lineno(1), "Variable not defined")
    except Exception as e:
        semantic_error(p.lineno(1), e)
    
def p_expression_const(p):
    """expression : const"""
    p[0] = p[1]
    
def p_expression_parentheses(p):
    """expression : '(' expression ')' """
    p[0] = p[2]

def p_const_int(p):
    """const : INTEGER"""
    p[0] = AST.Const('int', p[1])

def p_const_float(p):
    """const : FLOAT"""
    p[0] = AST.Const('float', p[1])

def p_const_string(p):
    """const : STRING"""
    p[0] = AST.Const('string', p[1])

def p_if_instr(p):
    """choice_instr : IF '(' condition ')' stmt %prec IFX"""
    try:
        p[0] = AST.IfStatement(p[3], p[5])
    except Exception as e:
        semantic_error(p.lineno(3), e)

def p_ifelse_instr(p):
    """choice_instr : IF '(' condition ')' stmt ELSE stmt """
    try:
        p[0] = AST.IfElseStatement(p[3], p[5], p[7])
    except Exception as e:
        semantic_error(p.lineno(3), e)
    
def p_while_instr(p):
    """while_instr : WHILE '(' condition ')' stmt """
    try:
        p[0] = AST.WhileStatement(p[3], p[5])
    except Exception as e:
        semantic_error(p.lineno(3), e)

def p_condition(p):
    """condition : expression EQ  expression
                 | expression NEQ expression
                 | expression GE  expression
                 | expression LE  expression
                 | expression '<' expression
                 | expression '>' expression """
    p[0] = AST.Expression(p[2], p[1], p[3])

def p_stmt(p):
    """stmt : assignment
            | choice_instr
            | while_instr """
    p[0] = p[1]
    
def p_stmt_block(p):
    """stmt : '{' instructions '}'"""
    p[0] = p[2]


file = open(sys.argv[1] if len(sys.argv) > 1 else "example.txt", "r");
lexer = lex.lex()
parser = yacc.yacc()
text = file.read()
parser.parse(text, lexer=lexer)
